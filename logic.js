function makeTable() {
    let form = document.frm;
    let rows = parseInt(form.rows.value);
    let columns = parseInt(form.columns.value);

    if(isNaN(rows) || isNaN(columns) || rows < 1 || columns < 1) {
        alert("Incorrect arguments");
    } else {
        let table = document.createElement("table");
        table.id = "table";
    
        for(let i = 1; i < rows + 1; i++){
            let elem = document.createElement("tr");
            for(let j = 1; j < columns + 1; j++){
                let cell = document.createElement("td");
                elem.appendChild(cell);
            }
            table.appendChild(elem);
        }
    
        document.body.appendChild(table);
    }
}

function tableExists() {
    let table = document.getElementById("table");
    
    if(table !== null){
        document.body.removeChild(table);
    }
}

let run = document.getElementById("run");
run.addEventListener("click", tableExists);
run.addEventListener("click", makeTable);
